# Add Infinity Search Firefox

Just our search tool for adding our service to the list of search options for Firefox users. 

Add us at [https://addons.mozilla.org/en-US/firefox/addon/add-infinity-search/](https://addons.mozilla.org/en-US/firefox/addon/add-infinity-search/).

